# Changelog
All notable changes to this job will be documented in this file.

## [0.2.1] - 2022-06-13
* Add docker image tag in variable 

## [0.1.1] - 2022-04-26
* Rename DOCUMENTATION_OUTPUT into OUTPUT_PATH
  
## [0.1.0] - 2021-12-17
* Initial version
