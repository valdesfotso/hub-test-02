# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2022-07-28
* Add docker image tag in variable 
* Add job author
## [0.1.0] - 2022-06-07
* Initial version